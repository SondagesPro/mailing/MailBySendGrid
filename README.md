# MailBySendGrid

A LimeSurvey plugin to use sendgrid for emailing.

## Home page & Copyright
- HomePage <http://extensions.sondages.pro/>
- Copyright © 2024 Denis Chenu <https://sondages.pro>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
- [Donate](https://support.sondages.pro/open.php?topicId=12), [Liberapay](https://liberapay.com/SondagesPro/), [OpenCollective](https://opencollective.com/sondagespro) 
