<?php

/**
 * @copyright 2024 Denis Chenu / Sondages Pro <https://sondages.pro>
 * @author Denis Chenu <denis@sondages.pro>
 * @license AGPL version 3
 * @version 0.1.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class MailBySendGrid extends PluginBase
{
    protected $storage = 'DbStorage';

    protected static $description = 'Use sendgrid to send emails and include optional categories.';
    protected static $name = 'MailBySendGrid';

    /** string[]|null current email replacement : array key is from email, value is result, used when multiple call */
    private static $currentEmailsReplace;
    /** string[]|null current email replacement : array key is from email, value is result, used when multiple call */
    private static $currentFromToEmailName;

    /** @var string|null string default email for sendgrid */
    private static $sendgridDefaultEmail;

    /** @var integer|null get the current survey id (for xsmtp api) */
    private static $currentSurveyId;
    /** @var string|null The string according to current smtpApi */
    private static $xSmtpApi = null;

    /** @inheritdoc, this plugin settings are update during getSettings */
    protected $settings = array(
        'emailDefaultType' => array(
            'type' => 'select',
            'label' => 'Email to use by defult',
            'options' => array(
                'siteadminemail' => 'Default site admin email',
                'siteadminbounce' => 'Default site bounce email',
                'adminemail' => 'Administrator email address or global survey settings',
                'mailbysendgrid' => 'Other, manually set',
            ),
            'help' => 'Choose the email set to From and Return-Path if not in the allowed list.',
            'default' => 'siteadminbounce',
        ),
        'emailManually' => array(
            'type' => 'string',
            'label' => 'Emails set if you choose other, manually set.',
            'default' => ''
        ),
        'allowedEmails' => array(
            'type' => 'text',
            'label' => 'Emails allwoed to send email, not updated.',
            'help' => 'See “Single Sender Verification“ in sendgrid configuration. One email by line, you can use comma (,) as separator when add a new one.' ,
            'default' => ''
        ),
        'allowedEmailDomains' => array(
            'type' => 'text',
            'label' => 'Emails allwoed to send email, not updated.',
            'help' => 'See “Domain Authentication“ in sendgrid configuration. One email by line, you can use comma (,) as separator when add a new one.',
            'default' => ''
        ),
        'addGroup' => array(
            'type' => 'select',
            'options' => array(
                'name' => 'Group code',
                'gsid' => 'Group id',
            ),
            'htmlOptions' => [
                'empty' => 'No',
            ],
            'label' => 'Add group as category',
            'help' => 'See <a href="https://docs.sendgrid.com/ui/analytics-and-reporting/categories" target="_blank">Category Statistics</a>.' ,
            'default' => 'name',
        ),
        'prefixGroup' => array(
            'type' => 'string',
            'label' => 'Prefix for group as category',
            'default' => '',
        ),
        'addSurvey' => array(
            'type' => 'select',
            'options' => array(
                'sid' => 'Survey id',
            ),
            'htmlOptions' => [
                'empty' => 'No',
            ],
            'label' => 'Add Survey as category',
            'default' => '',
        ),
        'prefixSurvey' => array(
            'type' => 'string',
            'label' => 'Prefix for Survey as category',
            'default' => 'survey:',
        ),
        'addOwner' => array(
            'type' => 'select',
            'options' => array(
                'users_name'=> 'Username',
                'uid' => 'User id',
            ),
            'htmlOptions' => [
                'empty' => 'No',
            ],
            'label' => 'Add owner as category',
            'default' => '',
        ),
        'prefixOwner' => array(
            'type' => 'string',
            'label' => 'Prefix for owner as category',
            'default' => 'user:',
        ),
    );

    /**
     * @inheritdoc, remove and fix settings for 3.X
     */
    public function getPluginSettings($getValues = true)
    {
        if (Yii::app() instanceof CConsoleApplication) {
            return;
        }
        $pluginSettings = parent::getPluginSettings($getValues);
        if (intval(Yii::app()->getConfig('versionnumber') < 4)) {
            unset($pluginSettings['emailDefaultType']['adminemail']);
        }
        return $pluginSettings;
    }

    /**
     * @inheritdoc, set allowedEmails and allowedEmailDomains to desired format
     */
    public function saveSettings($settings)
    {
        if (!Permission::model()->hasGlobalPermission('settings', 'update')) {
            throw new CHttpException(403);
        }
        if (isset($settings['emailManually'])) {
            $settings['emailManually'] = trim($settings['emailManually']);
            if (!filter_var($settings['emailManually'], FILTER_VALIDATE_EMAIL)) {
                $settings['emailManually'] = "";
            }
        }
        if (isset($settings['allowedEmails'])) {
            $allowedEmails = preg_split('/\r\n|\r|\n|,|;/', $settings['allowedEmails']);
            $allowedEmails = array_map(
                function ($allowedEmail) {
                    $allowedEmail = trim($allowedEmail);
                    if (!filter_var($allowedEmail, FILTER_VALIDATE_EMAIL)) {
                        return null;
                    }
                    return $allowedEmail;
                },
                $allowedEmails
            );
            $cronTypes = array_filter($allowedEmails);
            $settings['allowedEmails'] = implode("\n", $allowedEmails);
        }
        if (isset($settings['allowedEmailDomains'])) {
            $allowedEmailDomains = preg_split('/\r\n|\r|\n|,|;/', $settings['allowedEmailDomains']);
            $allowedEmailDomains = array_map(
                function ($allowedEmailDomain) {
                    $allowedEmailDomain = trim($allowedEmailDomain);
                    if (!filter_var("user@" . $allowedEmailDomain, FILTER_VALIDATE_EMAIL)) {
                        return null;
                    }
                    return $allowedEmailDomain;
                },
                $allowedEmailDomains
            );
            $allowedEmailDomains = array_filter($allowedEmailDomains);
            $settings['allowedEmailDomains'] = implode("\n", $allowedEmailDomains);
        }
        parent::saveSettings($settings);
    }
    /** @inheritdoc, this plugin didn't have any public method */
    public $allowedPublicMethods = array();

    /** @inheritdoc */
    public function init()
    {
        if (intval(App()->getConfig('versionnumber')) >= 4) {
            $this->subscribe('beforeEmail', 'sentSendGridEmail');
            $this->subscribe('beforeSurveyEmail', 'sentSendGridEmail');
            $this->subscribe('beforeTokenEmail', 'sentSendGridEmail');
        } else {
            $this->subscribe('beforeTokenEmailExtended', 'sentSendGridEmail');
        }
    }

    /** @see event */
    public function sentSendGridEmail()
    {
        $mailEvent = $this->getEvent();

        $limeMailer = $mailEvent->get('mailer');
        $surveyId = $mailEvent->get('survey', $mailEvent->get('surveyId', 0));

        /* 3.X didn't have mailer : potential eventBeforeTokenEmailExtended*/
        if (empty($limeMailer)) {
            $limeMailer = $mailEvent->get('PhpMailer');
        }
        if (empty($limeMailer)) {
            return;
        }
        list($fromEmail, $fromName) = $this->getFromMailName($mailEvent->get('from'));
        if ($fromEmail != self::getMailFixed($fromEmail)) {
            $limeMailer->AddReplyTo($fromEmail, $fromName);
            $mailFixed = self::getMailFixed($fromEmail);
            $limeMailer->From = $mailFixed;
            $mailEvent->set('from', $this->fixFrom($mailEvent->get('from'), $mailFixed));
        }
        $mailEvent->set('bounce', $mailEvent->get('bounce'));
        /* Disallow next update */
        $updateDisable = $this->getEvent()->get('updateDisable');
        $updateDisable['from'] = true;
        $updateDisable['bounce'] = true;
        $this->getEvent()->set('updateDisable', $updateDisable);
        
        /* Add categories */
        $xSmtpApi = $this->getXSmtpApi($surveyId);
        if ($xSmtpApi) {
            $limeMailer->addCustomHeader('x-smtpapi', $xSmtpApi);
        }
    }

    /**
     * Get the x-smtpapi for survey
     * @param $surveyuId
     * @return string
     */
    private function getXSmtpApi($surveyId)
    {
        if ($surveyId != self::$currentSurveyId) {
            self::$xSmtpApi = null;
        }
        self::$currentSurveyId = $surveyId;
        if (!is_null(self::$xSmtpApi)) {
            return self::$xSmtpApi;
        }
        self::$xSmtpApi = "";
        if (!$surveyId) {
           return self::$xSmtpApi;
        }
        $oSurvey = Survey::model()->findByPk($surveyId);
        if (!$oSurvey) {
           return self::$xSmtpApi;
        }
        $settings = $this->settings;
        $aStmpApiCategories = [];
        if ($addGroup = $this->get('addGroup', null, null, $settings['addGroup']['default'])) {
            $prefixGroup = $this->get('prefixGroup', null, null, $settings['prefixGroup']['default']);
            switch ($addGroup) {
                case 'name':
                    $oSurveyGroup = SurveysGroups::model()->findByPk($oSurvey->gsid);
                    if ($oSurveyGroup) {
                        $aStmpApiCategories[] = $prefixGroup . trim($oSurveyGroup->name);
                        /* Break only if valid */
                        break;
                    }
                    break;
                case 'gsid':
                default:
                    $aStmpApiCategories[] = $prefixGroup . $oSurvey->gsid;
            }
        }
        if ($addSurvey = $this->get('addSurvey', null, null, $settings['addSurvey']['default'])) {
            $prefixSurvey = $this->get('prefixSurvey', null, null, $settings['prefixSurvey']['default']);
            switch ($addSurvey) {
                case 'sid':
                default:
                    $aStmpApiCategories[] = $prefixSurvey . $oSurvey->sid;
            }
        }
        if ($addOwner = $this->get('addOwner', null, null, $settings['addOwner']['default'])) {
            $prefixOwner = $this->get('prefixOwner', null, null, $settings['prefixOwner']['default']);
            switch ($addOwner) {
                case 'users_name':
                    $oOwner = User::model()->findByPk($oSurvey->owner_id);
                    if ($oOwner) {
                        $aStmpApiCategories[] = $prefixOwner . trim($oOwner->users_name);
                        /* Break only if valid */
                        break;
                    }
                case 'uid':
                default:
                    $aStmpApiCategories[] = $prefixOwner . $oSurvey->owner_id;
            }
        }
        if (!empty($aStmpApiCategories)) {
            self::$xSmtpApi = json_encode(['category' => $aStmpApiCategories]);
        }
        return self::$xSmtpApi;
    }
    
    /**
     * get email from
     * @param string $email
     * @return string 
     */
    private function getMailFixed($email)
    {
        if (isset(self::$currentEmailsReplace[$email])) {
            return self::$currentEmailsReplace[$email];
        }
        self::$currentEmailsReplace[$email] = $email;
        $allowedEmails = explode("\n", $this->get('allowedEmails', null, null,''));
        if (in_array($email, $allowedEmails)) {
            return self::$currentEmailsReplace[$email];
        }
        $domain = substr($email, strpos($email, '@'));
        $allowedEmailDomains = explode("\n", $this->get('allowedEmailDomains', null, null,''));
        if (in_array($domain, $allowedEmailDomains)) {
            return self::$currentEmailsReplace[$email];
        }
        self::$currentEmailsReplace[$email] = $this->getDefaultSendgridEmail();
        return self::$currentEmailsReplace[$email];
    }
    
    /**
     * get final email replacement
     * @return string
     */
    private function getDefaultSendgridEmail()
    {
        if (isset(self::$sendgridDefaultEmail)) {
            return self::$sendgridDefaultEmail;
        }
        $emailDefaultType = $this->get("emailDefaultType", null, null, "siteadminbounce");
        $emailManually = $this->get("emailManually", null, null, "");

        self::$sendgridDefaultEmail = App()->getConfig("siteadminbounce");
        switch ($emailDefaultType) {
            case 'adminemail':
                $oSurveyGroupSetting = SurveysGroupsettings::model()->findByPk(0);
                if ($oSurveyGroupSetting) {
                    self::$sendgridDefaultEmail = $oSurveyGroupSetting->adminemail;
                }
                break;
            case 'siteadminemail':
                self::$sendgridDefaultEmail = App()->getConfig("siteadminemail");
                break;
            case 'mailbysendgrid':
                if ($emailManually) {
                    self::$sendgridDefaultEmail = $emailManually;
                }
                break;
            case 'siteadminbounce':
            default:
             // No update
        }
        return self::$sendgridDefaultEmail;
    }

    /**
     * Fix from string
     * @param $from string
     * @return array : [email, name];
     */
    private function getFromMailName($from)
    {
        if (isset(self::$currentFromToEmailName[$from])) {
            return self::$currentFromToEmailName[$from];
        }

        $fromemail = $from;
        $fromname = '';
        if (strpos($from, '<') !== false) {
            if (strpos($from, '<')) {
                $fromname = trim(substr($from, 0, strpos($from, '<') - 1));
            }
            $fromemail = substr($from, strpos($from, '<') + 1, strpos($from, '>') - 1 - strpos($from, '<'));
        }
        self::$currentFromToEmailName[$from] = [$fromemail, $fromname];        
        return self::$currentFromToEmailName[$from];
    }

    /**
     * Fix from string
     * @param $from string
     * @param $newFromEmail string
     * @return string
     */
    private function fixFrom($from, $newFromEmail)
    {
        list($fromemail, $fromname) = $this->getFromMailName($from);
        if (empty($fromname)) {
            // No name, or empty name : return only email
            return $newFromEmail;
        }
        return $fromname . " <" . $newFromEmail .">";
     }
}
